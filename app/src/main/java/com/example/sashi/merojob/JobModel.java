package com.example.sashi.merojob;

import java.util.List;

/**
 * Created by sashi on 9/8/2016.
 */
public class JobModel {

    /**
     * name : Php Developer
     * company : Peter Slevin
     * expirydate : 4th may
     * experience : 2
     * qualification : Batchlor
     * views : 71
     */

    private List<JobsBean> jobs;

    public List<JobsBean> getJobs() {
        return jobs;
    }

    public void setJobs(List<JobsBean> jobs) {
        this.jobs = jobs;
    }

    public static class JobsBean {
        private String name;
        private String company;
        private String expirydate;
        private String experience;
        private String qualification;
        private String views;

        public JobsBean(String name, String company, String expirydate, String qualification, String views, String experience) {
            this.name = name;
            this.company = company;
            this.expirydate = expirydate;
            this.qualification = qualification;
            this.views = views;
            this.experience = experience;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getExpirydate() {
            return expirydate;
        }

        public void setExpirydate(String expirydate) {
            this.expirydate = expirydate;
        }

        public String getExperience() {
            return experience;
        }

        public void setExperience(String experience) {
            this.experience = experience;
        }

        public String getQualification() {
            return qualification;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public String getViews() {
            return views;
        }

        public void setViews(String views) {
            this.views = views;
        }
    }
}
