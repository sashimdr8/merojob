package com.example.sashi.merojob;


import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by sashi on 9/8/2016.
 */
public interface RestInterFace {
    @GET("jobs.php")
    Call<List<JobModel>> getJobs();
 
}
