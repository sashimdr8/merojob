package com.example.sashi.merojob;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ListView listView;
    ArrayList<JobModel.JobsBean> jobslist;
    JobAdapter jobAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        jobslist = new ArrayList<>();
        listView = (ListView)findViewById(R.id.job_list);
        getJsonData();
       /* jobslist.add(new JobModel.JobsBean("Accountant","merojob","4th may","bachlor","23","1"));
        jobslist.add(new JobModel.JobsBean("Accountant","merojob","4th may","bachlor","23","1"));
        jobslist.add(new JobModel.JobsBean("Accountant","merojob","4th may","bachlor","23","1"));
        jobslist.add(new JobModel.JobsBean("Accountant","merojob","4th may","bachlor","23","1"));
        jobslist.add(new JobModel.JobsBean("Accountant","merojob","4th may","bachlor","23","1"));
        jobslist.add(new JobModel.JobsBean("Accountant","merojob","4th may","bachlor","23","1"));
        jobAdapter = new JobAdapter(MainActivity.this,jobslist);
        listView.setAdapter(jobAdapter);


*/

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public  void getJsonData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://post-test-neutronsudarshan.c9users.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterFace service = retrofit.create(RestInterFace.class);



        Call<List<JobModel>> categorycall = service.getJobs();
        categorycall.enqueue(new Callback<List<JobModel>>() {

            @Override
            public void onResponse(Response<List<JobModel>> response, Retrofit retrofit) {


                try {
                    int size = response.body().get(0).getJobs().size();
                    String job,company,expiry,views,experience,qualification;
                    for (int i=0; i< size; i++){
                        job = response.body().get(0).getJobs().get(i).getName();
                        company = response.body().get(0).getJobs().get(i).getCompany();
                        expiry = response.body().get(0).getJobs().get(i).getExpirydate();
                        experience = response.body().get(0).getJobs().get(i).getExperience();
                        qualification = response.body().get(0).getJobs().get(i).getQualification();
                        views = response.body().get(0).getJobs().get(i).getViews();
                        Log.d("value", "onResponse: "+job);
                        jobslist.add(new JobModel.JobsBean(job,company,expiry,qualification,views,experience));
                    }
                    jobAdapter = new JobAdapter(MainActivity.this,jobslist);
                    listView.setAdapter(jobAdapter);


                } catch (Exception e) {
                    Log.d("value", "Exception form try block ");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("value", "categoryNoooResponse: ");
            }
        });

    }
}
