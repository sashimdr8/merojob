package com.example.sashi.merojob;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sashi on 9/8/2016.
 */
public class JobAdapter extends BaseAdapter {
    ArrayList <JobModel.JobsBean> jobModels;
    Context context;
    TextView jobTitle,company,expiry,views;
    public JobAdapter(Context context, ArrayList<JobModel.JobsBean> jobModels) {
        this.context = context;
        this.jobModels = jobModels;
    }

    @Override
    public int getCount() {
        return jobModels.size();
    }

    @Override
    public Object getItem(int i) {
        return jobModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.indi_view,viewGroup, false);
        jobTitle = (TextView)rowView.findViewById(R.id.job_title);
        company = (TextView)rowView.findViewById(R.id.company);
        expiry = (TextView)rowView.findViewById(R.id.expiry);
        views = (TextView)rowView.findViewById(R.id.views);
        jobTitle.setText(jobModels.get(i).getName());
        company.setText(jobModels.get(i).getCompany());
        expiry.setText(jobModels.get(i).getExpirydate());
        views.setText(jobModels.get(i).getViews());


    return rowView;}
}
